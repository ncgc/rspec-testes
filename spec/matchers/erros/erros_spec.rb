require'calculator'

describe Calculator do

    context '#div erros' do
        it 'divide by 0' do
        expect{subject.div(3, 0)}.to raise_error(ZeroDivisionError)
        expect{subject.div(3, 0)}.to raise_error("divided by 0")
        expect{subject.div(3, 0)}.to raise_error(ZeroDivisionError, "divided by 0")
        expect{subject.div(3, 0)}.to raise_error(/divided/)
        end
    end
end