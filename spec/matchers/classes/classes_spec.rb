describe 'Classes' do
    it 'be_instance_of' do
        #Exatamente a classe
        expect(10).to be_instance_of(Integer)
    end

    it 'be_kind_of' do
        #Pode ser herança
        expect(10).to be_kind_of(Integer)
    end

    it 'respond_to' do
        expect("ruby").to respond_to(:size)
        expect("ruby").to respond_to(:count)
    end

    it 'be_an' do
        expect(10).to be_an(Integer)
        expect(10).to be_a(Integer)
    end
end