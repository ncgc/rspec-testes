describe 'Matchers de Igualdade' do
    it '> ' do
        expect(5).to be > 1
    end

    it '>= ' do
        expect(5).to be >= 2
        expect(5).to be >= 5
    end

    it '< ' do
        expect(5).to be < 10
    end

    it '<= ' do
        expect(5).not_to be <= 1
        expect(5).to be <= 8
    end

    it 'be_between' do
        expect(5).to be_between(2,5).inclusive
        expect(5).not_to be_between(2,5).exclusive
    end

    it 'match' do
        expect('fulano@email.com').to match(/..@../)
    end

    it 'start_with ' do
        expect('fulano e outros').to start_with('fulano')
        expect([1,2,3]).to start_with(1)
    end

    it 'end_with ' do
        expect('fulano e outros').to end_with('outros')
        expect([1,2,3]).to end_with(3)
    end

end
