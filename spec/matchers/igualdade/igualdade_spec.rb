describe 'Matchers de Igualdade' do
    it '#equal' do
        #Testa se os objetos são iguais
        x = "ruby"
        y = "ruby"
        expect(x).not_to equal(y)
        expect(x).to equal(x)
    end

    it '#be' do
        #Testa se os objetos são iguais
        x = "ruby"
        y = "ruby"
        expect(x).not_to be(y)
        expect(x).to be(x)
    end

    it '#eql' do
        #Testa se os valores dos objetos são iguais
        x = "ruby"
        y = "ruby"
        expect(x).to eql(y)
        expect(x).to eql(x)
    end

    it '#eq' do
        #Testa se os valores dos objetos são iguais
        x = "ruby"
        y = "ruby"
        expect(x).to eql(y)
        expect(x).to eql(x)
    end


end